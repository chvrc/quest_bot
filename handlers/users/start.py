from aiogram import types
from aiogram.dispatcher.filters.builtin import Command, CommandStart
from aiogram.dispatcher.storage import FSMContext
from aiogram.types.message import ContentType
from aiogram.utils.deep_linking import get_start_link, decode_payload
import time
from data.config import ADMINS
from keyboards.inline.keyboard import kb_help, kb_help2, kb_start
import asyncio

from loader import dp
from states.states import Question
from utils.db_api.database import add_user, edit_user_log, edit_user_start, edit_user_total, get_question, get_users, get_users_id
from utils.google_sheet.unload_data import unload_data


@dp.message_handler(content_types=ContentType.VIDEO, state='*', user_id=ADMINS)
async def bot_start(message: types.Message, state: FSMContext):
    await message.answer(message.video.file_id)


@dp.message_handler(content_types=ContentType.PHOTO, state='*', user_id=ADMINS)
async def bot_start(message: types.Message, state: FSMContext):
    await message.answer(message.photo[-1].file_id)


@dp.message_handler(Command('unload'), state='*', user_id=ADMINS)
async def bot_start(message: types.Message, state: FSMContext):
    users = get_users()
    await message.answer("Выгружаю...")
    unload_data(users)
    await message.answer("Выгружено")


@dp.message_handler(Command('mailing'), state='*', user_id=ADMINS)
async def bot_start(message: types.Message, state: FSMContext):
    await message.answer("Введите сообщение для рассылки")
    await Question.mailing.set()


@dp.message_handler(state=Question.mailing, user_id=ADMINS)
async def bot_start(message: types.Message, state: FSMContext):
    users = [1147013692, 202301879, 811535216, 371329515, 810115360,
            470754556, 402835624, 972348463, 262798731, 501986653]
    msg = message.text
    await message.answer("Выполняется рассылка...")
    for user in users:
        try:
            await dp.bot.send_message(user[0], msg)
            asyncio.sleep(0.1)
        except:
            await message.answer(f"Не удалось отправить сообщение ID {user[0]}")
    await message.answer("Рассылка выполнена")
    await state.finish()


@dp.message_handler(CommandStart())
async def bot_start(message: types.Message, state: FSMContext):
    msg1 = ("Привет! 👋 Ты пришла спасти Надю?\n"
            "Как тебя зовут? Напиши имя и фамилию 👤")
    await message.answer(msg1)
    await Question.login.set()


@dp.message_handler(state=Question.login)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text
    await message.answer("А где ты находишься? Напиши город и страну 🗺")
    await Question.city.set()


@dp.message_handler(state=Question.city)
async def bot_start(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    username = message.from_user.username
    city = message.text

    async with state.proxy() as data:
        name = data['name']
        data['city'] = city
        data['count_errors'] = 0
        data['count_help'] = 0

    add_user(user_id, username, name, city)
    
    msg1 = "Спасибо, {} 🙏🏼".format(name)
    msg2 = ("Не будем терять ни минуты! Пора переходить к делу.\n\n"
            "Сейчас ты приступишь к спасению Надежды. "
            "Если что-то не будет получаться, учти, "
            "что можно обратиться за помощью, тут есть "
            "подсказки к каждому заданию. Но имей ввиду, "
            "что за каждое обращение к подсказке к итоговому "
            "времени прохождения квеста будет добавлено 10 минут ⚠️\n\n"
            "Ты готова? Нажимай кнопку, время не ждет ⏰")
    kb = await kb_start()
    await message.answer(msg1)
    await message.answer(msg2, reply_markup=kb)
    await Question.q1.set()


@dp.callback_query_handler(lambda c: c.data == 'start', state=Question.q1)
async def ask_question(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['time_start'] = time.time()
    user_id = call.from_user.id
    edit_user_start(user_id, time.time())
    msg1 = "Прекрасно. Переходим к имеющимся данным 🚀"
    msg2 = "Для перехода к следующему заданию введите код."
    msg3 = "Формат ответа: Слово 🤖"
    await call.message.edit_reply_markup()
    await call.message.answer(msg1)
    await call.message.answer("Задание 1")
    await call.message.answer_video(video="BAACAgIAAxkBAAMGYbEjhLuo0MBqL8ipXBJsb515Pw0AAo8UAAK8G4lJc9-BpOI6npEjBA")
    kb = await kb_help(1)
    await call.message.answer(msg2, reply_markup=kb)
    await call.message.answer(msg3)
    await Question.q2.set()



@dp.message_handler(state=Question.q2)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        name = data['name']
        text = message.text.lower()
        answers = ["помогите"]
        msg1 = "Отличная работа! {}, мы знали, что на тебя можно положиться. Движемся дальше 💪".format(name)
        msg2 = "Ну что, какие мысли? Где сейчас Надя?"
        msg3 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "2")
            kb = await kb_help(2)
            await message.answer(msg1)
            await message.answer("Задание 2")
            await message.answer_video(video="BAACAgIAAxkBAAIDoWGxuQPYf0SQeNSGWxe9xxq8S3A9AAJkEAACgt2QSQ9wn0ZbkV6oIwQ")
            await message.answer(msg2, reply_markup=kb)
            await message.answer(msg3)
            await Question.q3.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q3)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["патриаршие", "патрики", "маяковская", "патриаршие пруды"]
        msg1 = "Код принят ✅"
        msg2 = ("Наде нужна Ваша помощь! Кажется, сумасшедшая в розовом платье хочет "
                "залить бетонным стайлингом полгорода 😭 "
                "Внимательно посмотрите видео и напишите, сколько банок лака она положила в тележку?")
        msg3 = "Формат ответа: Число 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "3")
            await message.answer(msg1)
            await message.answer("Задание 3")
            await message.answer_video(video="BAACAgIAAxkBAAMsYbEmA-v8wKEOOwezd7ZhXO7aUcEAApoUAAK8G4lJcSDRO0oWrs4jBA")
            kb = await kb_help(3)
            await message.answer(msg2, reply_markup=kb)
            await message.answer(msg3)
            await Question.q4.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q4)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["13", "тринадцать"]
        msg1 = "Совершенно верно! Зачем ей столько? 😱"
        msg2 = ("Ребята! У меня очень мало времени 😭 С видео временно накладки. "
                "Передаю Вам информацию в виде шифра. Используйте наш внутренний словарь! "
                "Мне не справиться без Вас 🙏🏼")
        msg3 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "4")
            media = types.MediaGroup()
            media.attach_photo("AgACAgIAAxkBAAMuYbEnEX1sxhyo7WRSykdgOGaVc44AAu-6MRuDEnFJ6JG5_5PQH9cBAAMCAAN4AAMjBA")
            media.attach_photo("AgACAgIAAxkBAAMwYbEnInuit5EpGqWFrTs70xqzsCIAAvC6MRuDEnFJBoPPOJS6Eq8BAAMCAANtAAMjBA")
            media.attach_photo("AgACAgIAAxkBAAMyYbEnKrhFmi2wWBdb8Ysfebl9fRYAAvG6MRuDEnFJgJ4nyL52wE8BAAMCAAN5AAMjBA")
            media.attach_photo("AgACAgIAAxkBAAM0YbEnNTyok0YnnTeNU3ClOv3muNwAAvK6MRuDEnFJSG4FWtjNz0sBAAMCAAN4AAMjBA")
            media.attach_photo("AgACAgIAAxkBAAM2YbEnPQ7t4pcdeIGYJPTVvAL05aMAAvO6MRuDEnFJEruDgvrlebQBAAMCAAN4AAMjBA")
            await message.answer(msg1)
            await message.answer("Задание 4")
            await message.answer_media_group(media=media)
            kb = await kb_help(4)
            await message.answer(msg2, reply_markup=kb)
            await message.answer(msg3)
            await Question.q5.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q5)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        name = data['name']
        text = message.text.lower()
        answers = ["клише"]
        msg1 = "{}, мы справились в очередной раз 👏".format(name)
        msg2 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "5")
            await message.answer(msg1)
            await message.answer("Задание 5")
            kb = await kb_help(5)
            await message.answer_video(video="BAACAgIAAxkBAAOsYbFQxSvLnp9Ff-QhK2w6DT6r-mcAApcUAAK8G4lJhD3R12Xfp1QjBA", reply_markup=kb)
            await message.answer(msg2)
            await Question.q6.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q6)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        name = data['name']
        text = message.text.lower()
        answers = ["завитушка"]
        msg1 = "Браво, {}! Надеемся, что конец близок 🙏🏼".format(name)
        msg2 = "А где еще может спрятать свою информацию инстаблогер? 👁️"
        msg3 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "6")
            await message.answer(msg1)
            await message.answer("Задание 6")
            await message.answer_video(video="BAACAgIAAxkBAAOuYbFQz2FZTK7Tu6vp0ulCC8ERKIsAApgUAAK8G4lJtUWQvyqX0mEjBA")
            kb = await kb_help(6)
            await message.answer(msg2, reply_markup=kb)
            await message.answer(msg3)
            await Question.q7_1.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q7_1)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["стайлинг"]
        msg1 = "Код принят ✅"
        msg2 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "7.1")
            await message.answer(msg1)
            await message.answer("Задание 7.1")
            await message.answer_video(video="BAACAgIAAxkBAAOwYbFQ14hlhf8xhFOkWRc6qv1A12QAApUUAAK8G4lJ2sTpHfTwGZ4jBA")
            await message.answer_photo(photo="AgACAgIAAxkBAAO4YbFUmvyv5fKFR71H5qcdWCYXrKUAAny5MRu8G4lJpT113ItT__oBAAMCAAN5AAMjBA")
            kb = await kb_help(7)
            await message.answer(msg2, reply_markup=kb)
            await Question.q7_2.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q7_2)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["кулинария"]
        msg1 = "Код принят ✅"
        msg2 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "7.2")
            await message.answer(msg1)
            await message.answer("Задание 7.2")
            await message.answer_photo(photo="AgACAgIAAxkBAAO6YbFUqbArbNmff6fPgcfRX1_EHq4AAn25MRu8G4lJAv90bkakZvABAAMCAAN5AAMjBA")
            kb = await kb_help(7)
            await message.answer(msg2, reply_markup=kb)
            await Question.q7_3.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q7_3)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["шляпа", "шапка"]
        msg1 = "Код принят ✅"
        msg2 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "7.3")
            await message.answer(msg1)
            await message.answer("Задание 7.3")
            await message.answer_photo(photo="AgACAgIAAxkBAAO8YbFUtPyXxf18BMtqx6hJ7deqkW0AAn65MRu8G4lJKvDYR8H27mQBAAMCAAN5AAMjBA")
            kb = await kb_help(7)
            await message.answer(msg2, reply_markup=kb)
            await Question.q7_4.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q7_4)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["зеленый", "зелёный"]
        msg1 = "Код принят ✅"
        msg2 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "7.4")
            await message.answer(msg1)
            await message.answer("Задание 7.4")
            await message.answer_photo(photo="AgACAgIAAxkBAAO-YbFUu8BS9J8yK18MxQY5AAGSE6SmAAJ_uTEbvBuJSeTwerBVLAABTwEAAwIAA3kAAyME")
            kb = await kb_help(7)
            await message.answer(msg2, reply_markup=kb)
            await Question.q8.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q8)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["ромашка"]
        msg1 = "Код принят ✅"
        msg2 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "8")
            await message.answer(msg1)
            await message.answer("Задание 8")
            await message.answer_video(video="BAACAgIAAxkBAAOyYbFQ4V8uxmzhVoS3labX2RzVE28AApQUAAK8G4lJ5UuQYM2lx8ojBA")
            await message.answer_photo(photo="AgACAgIAAxkBAAP4YbFa5kjCAqECCGm0wM0CX_CySAUAAle5MRuC3YhJ2S9pf77DCDsBAAMCAAN5AAMjBA")
            kb = await kb_help(8)
            await message.answer(msg2, reply_markup=kb)
            await Question.q9.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.q9)
async def bot_start(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["beautiful"]
        msg1 = "Код принят ✅"
        msg2 = "Формат ответа: Слово 🤖"
        if text in answers:
            user_id = message.from_user.id
            edit_user_log(user_id, "9")
            await message.answer(msg1)
            await message.answer("Задание 9")
            kb = await kb_help(9)
            await message.answer_video(video="BAACAgIAAxkBAAO0YbFQ8XBVHBCRRhWzcRN7vLQxjGoAApkUAAK8G4lJSvaNvZ04jj8jBA", reply_markup=kb)
            await message.answer(msg2)
            await Question.end.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.message_handler(state=Question.end)
async def bot_start(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    username = message.from_user.username
    time_finish = time.time()
    async with state.proxy() as data:
        text = message.text.lower()
        answers = ["простим", "прости", "прощаем", "да", "конечно", "ок", "окей", "ok"]
        msg1 = ("Ура! 🍾"
                "Спасибо за участие в этом квесте. "
                "Будем ждать обратную связь в комментариях к последнему посту Надежды ♥️\n"
                "<a href=\"https://www.instagram.com/makeuptrend/p/CXO3QhwJPB9/?utm_medium=copy_link\">Ссылка на пост</a>")
        if text in answers:
            user_id = message.from_user.id
            time_start = data['time_start']
            count_help = data['count_help']
            count_errors = data['count_errors']
            time_total = time_finish - time_start + count_help * 60 * 10
            edit_user_total(user_id, time_finish, time_total, count_errors, count_help)
            await message.answer_video(video="BAACAgIAAxkBAAO2YbFQ_jinb3BEfT-hc8ZNeYTlwS8AAp8UAAK8G4lJW1FbHmFjNMgjBA")
            await message.answer(msg1)
            await Question.finish.set()
        else:
            data['count_errors'] += 1
            await message.answer("Некорректные данные ❌")


@dp.callback_query_handler(lambda c: c.data.startswith('help'), state='*')
async def ask_question(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['count_help'] += 1
    qid = int(call.data[4:])
    if qid == 1:
        help_msg = ("Подсказка 1 🔍\n"
                    "Воспользуйтесь азбукой Глухонемых.")
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg)
    elif qid == 2:
        help_msg = ("Подсказка 2 🔍\n"
                    "С этого места начинается \"Мастер и Маргарита\".")
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg)
    elif qid == 3:
        help_msg = ("Подсказка 3 🔍\n"
                    "Напишите, сколько банок лака сумасшедшая положила в тележку.")
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg)
    elif qid == 4:
        help_msg = ("Подсказка 4-1 🔍\n"
                    "На картинках изображены «ошибки и причёски из словаря Makeuptrend». Используйте первые буквы в названиях.")
        kb = await kb_help2()
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg, reply_markup=kb)
    elif qid == 5:
        help_msg = ("Подсказка 5 🔍\n"
                    "Составьте слово из полученных букв на стене.")
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg)
    elif qid == 6:
        help_msg = ("Подсказка 6 🔍\n"
                    "Используйте Инстаграм Нади. Первая цифра - номер поста. "
                    "Вторая - номер буквы в посте.")
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg)
    elif qid == 7:
        help_msg = ("Подсказка 7 🔍\n"
                    "Используйте Яндекс.Панорамы и следуйте по маршруту, указаном в траектории.")
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg)
    elif qid == 8:
        help_msg = ("Подсказка 8 🔍\n"
                    "Используйте нужные геометрические фигуры из сторис, чтобы понять, какая буква Вам необходима.")
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg)
    elif qid == 9:
        help_msg = ("Подсказка 9 🔍\n"
                    "Простим или нет? 😇")
        await call.message.edit_reply_markup()
        await call.message.answer(help_msg)


@dp.callback_query_handler(lambda c: c.data == 'hhelp', state='*')
async def ask_question(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['count_help'] += 1
    help_msg = ("Подсказка 4-2 🔍\n"
                "Колтуны, Локон страсти, Императрица, Шлем велосипедиста, Единорог.")
    await call.message.edit_reply_markup()
    await call.message.answer(help_msg)
