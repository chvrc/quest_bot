from aiogram.types import (InlineKeyboardMarkup, InlineKeyboardButton, 
                            ReplyKeyboardMarkup, KeyboardButton)

from data.config import ADMINS


async def kb_admin():
    kb = InlineKeyboardMarkup()
    bt1 = InlineKeyboardButton('Добавить', callback_data='add')
    kb.add(bt1)
    return kb


async def kb_start():
    kb = InlineKeyboardMarkup()
    bt = InlineKeyboardButton('Спасти', callback_data="start")
    kb.add(bt)
    return kb


async def kb_help(qid):
    kb = InlineKeyboardMarkup()
    bt = InlineKeyboardButton('💡 Подсказка', callback_data=f"help{qid}")
    kb.add(bt)
    return kb


async def kb_help2():
    kb = InlineKeyboardMarkup()
    bt = InlineKeyboardButton('💡 Ещё подсказка', callback_data="hhelp")
    kb.add(bt)
    return kb