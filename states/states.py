from aiogram.dispatcher.filters.state import State, StatesGroup


class Question(StatesGroup):
    login = State()
    city = State()
    q1 = State()
    q2 = State()
    q3 = State()
    q4 = State()
    q5 = State()
    q6 = State()
    q7_1 = State()
    q7_2 = State()
    q7_3 = State()
    q7_4 = State()
    q8 = State()
    q9 = State()
    end = State()
    finish = State()
    mailing = State()

