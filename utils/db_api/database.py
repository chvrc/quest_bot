import sqlite3
from datetime import datetime


def create_db():
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute(
    """
        CREATE TABLE IF NOT EXISTS users(
        id INTEGER PRIMARY KEY,
        user_id INTEGER UNIQUE,
        username TEXT,
        name TEXT,
        city TEXT,
        time_start REAL,
        time_finish REAL,
        time_total REAL,
        count_errors INTEGER,
        count_help INTEGER,
        log TEXT)
    """)
    cur.execute(
    """
        CREATE TABLE IF NOT EXISTS questions(
        id INTEGER PRIMARY KEY,
        text TEXT,
        answer TEXT,
        content_type INTEGER,
        content TEXT,
        error_msg TEXT,
        help_msg TEXT)
    """)
    cur.execute(
    """
        CREATE TABLE IF NOT EXISTS messages(
        id INTEGER PRIMARY KEY,
        message TEXT)
    """)


def add_user(user_id, username, name, city):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE
                INTO users(user_id, username, name, city)
                VALUES(?, ?, ?, ?);
                """, (user_id, username, name, city))
    conn.commit()


def edit_user_log(user_id, log):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                UPDATE users
                SET log = ?
                WHERE user_id = ?
                """, (log, user_id))
    conn.commit()


def edit_user_start(user_id, time_start):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                UPDATE users
                SET time_start = ?, log = "1"
                WHERE user_id = ?
                """, (time_start, user_id))
    conn.commit()


def edit_user_total(user_id, time_finish, time_total, count_errors, count_help):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                UPDATE users
                SET time_finish = ?, time_total = ?, count_errors = ?, count_help = ?, log = "DONE"
                WHERE user_id = ?
                """, (time_finish, time_total, count_errors, count_help, user_id))
    conn.commit()


def get_user(user_id):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""SELECT * from users
                    WHERE user_id = ?;
                    """, (user_id,))
    res = cur.fetchone()
    conn.commit()
    return res


def get_users_id():
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""SELECT user_id from users""")
    res = cur.fetchall()
    conn.commit()
    return res


def get_users():
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""SELECT * from users""")
    res = cur.fetchall()
    conn.commit()
    arr = []
    arr.append(("ID", "USERNAME", "NAME", "LOCATION", "TIME_START", "TIME_END", "TIME_TOTAL", "COUNT_ERRORS", "COUNT_HELP", "STEP"))
    for row in res:
        if row[6]:
            arr.append((row[1], row[2], row[3], row[4], 
                datetime.utcfromtimestamp(row[5]).strftime('%d-%m-%Y %H:%M:%S'), 
                datetime.utcfromtimestamp(row[6]).strftime('%d-%m-%Y %H:%M:%S'), 
                row[7], row[8], row[9], row[10]))
        elif row[5]:
            arr.append((row[1], row[2], row[3], row[4], 
                datetime.utcfromtimestamp(row[5]).strftime('%d-%m-%Y %H:%M:%S'), 
                row[6],
                row[7], row[8], row[9], row[10]))
        else:
            arr.append((row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))
    return arr


# ---------- Сообщения ----------
def add_msg(msg):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR REPLACE
                INTO messages(message)
                VALUES(?)
                """, (msg, ))
    conn.commit()


def get_msg(msg_id):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""SELECT message from messages
                    WHERE id = ?;
                    """, (msg_id, ))
    res = cur.fetchone()
    conn.commit()
    return res


# ---------- Вопросы ----------
def add_question(text, answer, content_type, content, error_msg, help_msg):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR REPLACE
                INTO questions(text, answer, content_type, content, error_msg, help_msg)
                VALUES(?, ?, ?, ?, ?, ?)
                """, (text, answer, content_type, content, error_msg, help_msg))
    conn.commit()


def get_question(question_id):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""SELECT * from questions
                    WHERE id = ?;
                    """, (question_id,))
    res = cur.fetchone()
    conn.commit()
    return res